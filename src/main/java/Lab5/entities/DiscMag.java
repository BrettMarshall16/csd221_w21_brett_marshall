package Lab5.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author Java
 */

@Entity
public class DiscMag extends Magazine implements Serializable {

    @Basic
    private String hasDisc;

    public String getHasDisc() {
        return hasDisc;
    }

    public void setHasDisc(String hasDisc) {
        this.hasDisc = hasDisc;
    }

}