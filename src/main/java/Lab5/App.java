/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab5;

import Lab5.Controllers.BookJpaController;
import Lab5.Controllers.MagazineJpaController;
import Lab5.entities.*;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Java
 */
public class App {
    public void run() throws Exception{
        //brett_DEFAULT_PU
        
       
        String theMenu = "1. Add A Book\n" + "2.Edit a Book\n" +
                         "3.Delete a Book\n" + "4.Add Magazine\n" +
                         "5.Add Disc Magazine \n" + "6.List Disc Magazine\n" + "7.Quit";
        boolean quit = false;
        Scanner input = new Scanner(System.in);
        
        while(!quit){
            System.out.println(theMenu);
            int option = input.nextInt();
            
            switch(option){
                case 1:
                    addBook();
                    break;
                case 2:
                    editBook();
                    break;
                case 3:
                    deleteBook();
                    break;
                case 4:
                    addMagazine();
                    break;
                case 5:
                    editMagazine();
                    break;
                case 6:
                    deleteMagazine();
                    break;
            }
            
        }
    }   
     public void addBook(){
        EntityManagerFactory emf=null;
        EntityManager em=null;
        emf = Persistence.createEntityManagerFactory("LAB5_PU");
        em = emf.createEntityManager();
        BookJpaController bookController = new BookJpaController(emf);
         Scanner input = new Scanner(System.in);
         System.out.println("Enter Author: ");
         String author = input.nextLine();
         System.out.println("Quantity to Order: ");
         String orderQty = input.nextLine();
         System.out.println("Ener a Title: ");
         String title = input.nextLine();
         System.out.println("Enter a Price: ");
         String price = input.nextLine(); 
          Book x = new Book();
          x.setAuthor(author);
          x.setTitle(title);
          x.setPrice(price);
          x.setCopies(orderQty);
          bookController.create(x);
     }
      public void editBook() throws Exception{
        EntityManagerFactory emf=null;
        EntityManager em=null;
        emf = Persistence.createEntityManagerFactory("LAB5_PU");
        em = emf.createEntityManager();
        BookJpaController bookController = new BookJpaController(emf);
         List<Book> bookList = bookController.findBookEntities();
         int counter = 1;
            for(Book book:bookList){
                System.out.println(counter + ". "+ book.getTitle());
                counter++;
            }
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the book you want to edit: ");
        int index = input.nextInt();
        Book x = bookList.get(index-1);
        System.out.println("Enter the new price for the book: ");
        String newPrice = input.nextLine();
        x.setPrice(newPrice);
        bookController.edit(x);
     }
      public void deleteBook() throws Exception{
        EntityManagerFactory emf=null;
        EntityManager em=null;
        emf = Persistence.createEntityManagerFactory("LAB5_PU");
        em = emf.createEntityManager();
        BookJpaController bookController = new BookJpaController(emf);
         List<Book> bookList = bookController.findBookEntities();
         int counter = 1;
            for(Book book:bookList){
                System.out.println(counter + ". "+ book.getTitle());
                counter++;
            }
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the book you want to Delete: ");
        int index = input.nextInt();
        Book x = bookList.get(index-1);
        bookController.destroy(x.getId());
     }
      public void addMagazine(){
        EntityManagerFactory emf=null;
        EntityManager em=null;
        emf = Persistence.createEntityManagerFactory("LAB5_PU");
        em = emf.createEntityManager();
        MagazineJpaController magazineController = new MagazineJpaController(emf);
         Scanner input = new Scanner(System.in);
         System.out.println("Enter a Publish Date: ");
         String publishedDate = input.nextLine();
         System.out.println("Quantity to Order: ");
         String orderQty = input.nextLine();
         System.out.println("Ener a Title: ");
         String title = input.nextLine();
         System.out.println("Enter a Price: ");
         String price = input.nextLine(); 
          Magazine x = new Magazine();
          x.setPublishedDate(publishedDate);
          x.setTitle(title);
          x.setPrice(price);
          x.setCopies(orderQty);
          magazineController.create(x);
     }
      public void editMagazine() throws Exception{
        EntityManagerFactory emf=null;
        EntityManager em=null;
        emf = Persistence.createEntityManagerFactory("LAB5_PU");
        em = emf.createEntityManager();
        MagazineJpaController magazineController = new MagazineJpaController(emf);
         List<Magazine> magazineList = magazineController.findMagazineEntities();
         int counter = 1;
            for(Magazine Mag:magazineList){
                System.out.println(counter + ". "+ Mag.getTitle());
                counter++;
            }
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the Magazine you want to edit: ");
        int index = input.nextInt();
        Magazine x = magazineList.get(index-1);
        System.out.println("Enter the new price for the Magazine: ");
        String newPrice = input.nextLine();
        x.setPrice(newPrice);
        magazineController.edit(x);
     }
      public void deleteMagazine() throws Exception{
        EntityManagerFactory emf=null;
        EntityManager em=null;
        emf = Persistence.createEntityManagerFactory("LAB5_PU");
        em = emf.createEntityManager();
        MagazineJpaController magazineController = new MagazineJpaController(emf);
         List<Magazine> magazineList = magazineController.findMagazineEntities();
         int counter = 1;
            for(Magazine Mag:magazineList){
                System.out.println(counter + ". "+ Mag.getTitle());
                counter++;
            }
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the Magazine you want to Delete: ");
        int index = input.nextInt();
        Magazine x = magazineList.get(index-1);
        magazineController.destroy(x.getId());
     }

}
