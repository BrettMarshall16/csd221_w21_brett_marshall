/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab1.q6;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 17052
 */
public class App {
    ArrayList<Person> people = new ArrayList<>();
    public void run(){
        String menu = "1. Add a Person\n" + "2. List People\n" + "3. Delete a person\n" + "99. Exit\n";
        Scanner input = new Scanner(System.in);
        boolean quit = false;
        
        while(!quit){
            System.out.println(menu);
            int option = input.nextInt();
            
            switch(option){
                case 1:
                    addPerson();
                    break;
                case 2:
                    listPeople();
                    break;
                case 3:
                    deletePerson();
                    break;
                case 99:
                    quit = true;
                    System.exit(1);
            }
        }
    }
    public void addPerson(){
            Scanner input = new Scanner(System.in);
            System.out.println("Enter the SIN Number:");
            int tempsin = input.nextInt();
            System.out.println("Enter the first name:");
            String tempFirstName = input.next();
            System.out.println("Enter the Last name:");
            String tempLastName = input.next();
            Person x = new Person(tempsin, tempFirstName, tempLastName);
            people.add(x);
    }
    
    public void listPeople(){
        for(int i = 0; i < people.size(); i++){
            System.out.print(people.get(i));
        }
    }
    
    private void deletePerson(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the SIN of the person your want to delete: ");
        int tempsin = input.nextInt();
        for(int i = 0; i < people.size(); i++){
            if (people.get(i).getSIN() == tempsin){
                people.remove(i);
                break;
            }
        }
    }
    
}
