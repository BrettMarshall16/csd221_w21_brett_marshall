/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab1.q6;

/**
 *
 * @author 17052
 */
public class Person {
    private int SIN;
    private String firstName;
    private String lastName;
    
    public Person(){

    }
    
    public Person(int SIN, String firstName, String lastName){
        this.SIN = SIN;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getSIN() {
        return SIN;
    }

    public void setSIN(int SIN) {
        this.SIN = SIN;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public boolean equals(Object o){
        if(! (o instanceof Person))
            return false;
        Person x=(Person)o;
        if(x.SIN == this.SIN)
                return true;
       return false;
    }

     public String toString(){
        return "First Name " + firstName + "\n" + "Last Name:" + lastName + "\n" + "SIN: " + SIN;
    }

    
    
}