package brett_marshall_test4_practical.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author Java
 */
@Entity
public abstract class Square_Marshall extends Shape_Marshall {

    @Basic
    private double width;
    @Basic
    private double thelength;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getThelength() {
        return thelength;
    }

    public void setThelength(double thelength) {
        this.thelength = thelength;
    }
    

}