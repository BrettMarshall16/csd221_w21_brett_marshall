/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brett_marshall_test4_practical.entities;

/**
 *
 * @author Java
 */
public interface ShapeInterface {
    default public void printArea(double x, double y){
        System.out.println("The area is: " + (x*y));
    };
}
