package brett_marshall_test4_practical.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author Java
 */
@Entity
public class Triangle_Marshall extends Shape_Marshall {

    @Basic
    private double base;
    @Basic
    private double height;

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void printArea(double x, double y) {
        double area = x * y;
        System.out.println("The Area is: " + area);
    }

    public void printArea() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}