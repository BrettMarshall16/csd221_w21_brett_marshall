package brett_marshall_test4_practical.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Java
 */
@Entity
public abstract class Shape_Marshall implements Serializable,ShapeInterface{

    @Id
    @GeneratedValue
    private Long id;
    @Basic
    private double area;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

}