/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brett_marshall_test4_practical;

import brett_marshall_test4_practical.entities.Triangle_Marshall;
import brett_marshall_test4_practical.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Java
 */
public class App {
    public void run(){
        EntityManagerFactory emf=null;
        EntityManager em=null;
        
        try{
            emf=Persistence.createEntityManagerFactory("brett_marshall_test4_PU");
            em=emf.createEntityManager();
            Logger.getLogger(brett_marshall_test4_practical.Main.class.getName()).log(Level.INFO, "Entity Manager created ("+emf+")");
            em.getTransaction().begin();
            
            Triangle_Marshall x = new Triangle_Marshall();
            x.setBase(10);
            x.setHeight(10);
            em.persist(x);
            
            
            em.getTransaction().commit();
        }catch(Exception e){
            Logger.getLogger(Lab5.Main.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            if(emf!=null)
                emf.close();
        }
        
    
    }
}
