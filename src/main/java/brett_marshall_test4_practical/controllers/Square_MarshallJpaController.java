/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brett_marshall_test4_practical.controllers;

import brett_marshall_test4_practical.entities.Square_Marshall;
import brett_marshall_test4_practical.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Java
 */
public class Square_MarshallJpaController implements Serializable {

    public Square_MarshallJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Square_Marshall square_Marshall) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(square_Marshall);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Square_Marshall square_Marshall) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            square_Marshall = em.merge(square_Marshall);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = square_Marshall.getId();
                if (findSquare_Marshall(id) == null) {
                    throw new NonexistentEntityException("The square_Marshall with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Square_Marshall square_Marshall;
            try {
                square_Marshall = em.getReference(Square_Marshall.class, id);
                square_Marshall.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The square_Marshall with id " + id + " no longer exists.", enfe);
            }
            em.remove(square_Marshall);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Square_Marshall> findSquare_MarshallEntities() {
        return findSquare_MarshallEntities(true, -1, -1);
    }

    public List<Square_Marshall> findSquare_MarshallEntities(int maxResults, int firstResult) {
        return findSquare_MarshallEntities(false, maxResults, firstResult);
    }

    private List<Square_Marshall> findSquare_MarshallEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Square_Marshall as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Square_Marshall findSquare_Marshall(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Square_Marshall.class, id);
        } finally {
            em.close();
        }
    }

    public int getSquare_MarshallCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Square_Marshall as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
