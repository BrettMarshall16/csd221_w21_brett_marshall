/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brett_marshall_test4_practical.controllers;

import brett_marshall_test4_practical.entities.Triangle_Marshall;
import brett_marshall_test4_practical.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Java
 */
public class Triangle_MarshallJpaController implements Serializable {

    public Triangle_MarshallJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Triangle_Marshall triangle_Marshall) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(triangle_Marshall);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Triangle_Marshall triangle_Marshall) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            triangle_Marshall = em.merge(triangle_Marshall);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = triangle_Marshall.getId();
                if (findTriangle_Marshall(id) == null) {
                    throw new NonexistentEntityException("The triangle_Marshall with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Triangle_Marshall triangle_Marshall;
            try {
                triangle_Marshall = em.getReference(Triangle_Marshall.class, id);
                triangle_Marshall.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The triangle_Marshall with id " + id + " no longer exists.", enfe);
            }
            em.remove(triangle_Marshall);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Triangle_Marshall> findTriangle_MarshallEntities() {
        return findTriangle_MarshallEntities(true, -1, -1);
    }

    public List<Triangle_Marshall> findTriangle_MarshallEntities(int maxResults, int firstResult) {
        return findTriangle_MarshallEntities(false, maxResults, firstResult);
    }

    private List<Triangle_Marshall> findTriangle_MarshallEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Triangle_Marshall as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Triangle_Marshall findTriangle_Marshall(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Triangle_Marshall.class, id);
        } finally {
            em.close();
        }
    }

    public int getTriangle_MarshallCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Triangle_Marshall as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
