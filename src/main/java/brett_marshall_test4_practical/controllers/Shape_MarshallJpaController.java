/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brett_marshall_test4_practical.controllers;

import brett_marshall_test4_practical.entities.Shape_Marshall;
import brett_marshall_test4_practical.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Java
 */
public class Shape_MarshallJpaController implements Serializable {

    public Shape_MarshallJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Shape_Marshall shape_Marshall) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(shape_Marshall);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Shape_Marshall shape_Marshall) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            shape_Marshall = em.merge(shape_Marshall);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = shape_Marshall.getId();
                if (findShape_Marshall(id) == null) {
                    throw new NonexistentEntityException("The shape_Marshall with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Shape_Marshall shape_Marshall;
            try {
                shape_Marshall = em.getReference(Shape_Marshall.class, id);
                shape_Marshall.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The shape_Marshall with id " + id + " no longer exists.", enfe);
            }
            em.remove(shape_Marshall);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Shape_Marshall> findShape_MarshallEntities() {
        return findShape_MarshallEntities(true, -1, -1);
    }

    public List<Shape_Marshall> findShape_MarshallEntities(int maxResults, int firstResult) {
        return findShape_MarshallEntities(false, maxResults, firstResult);
    }

    private List<Shape_Marshall> findShape_MarshallEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Shape_Marshall as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Shape_Marshall findShape_Marshall(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Shape_Marshall.class, id);
        } finally {
            em.close();
        }
    }

    public int getShape_MarshallCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Shape_Marshall as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
