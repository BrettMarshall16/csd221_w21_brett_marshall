/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brett_marshall_test4_practical;
import brett_marshall_test4_practical.controllers.Square_MarshallJpaController;
import brett_marshall_test4_practical.controllers.Triangle_MarshallJpaController;
import brett_marshall_test4_practical.controllers.exceptions.NonexistentEntityException;
import brett_marshall_test4_practical.entities.Square_Marshall;
import brett_marshall_test4_practical.entities.Triangle_Marshall;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Java
 */
public class AppWithControllers {
    EntityManagerFactory emf = null;
    EntityManager em = null;
    public void run() throws NonexistentEntityException, Exception{
        emf = Persistence.createEntityManagerFactory("brett_marshall_test4_PU");
        em = emf.createEntityManager();
        Triangle_MarshallJpaController sm = new Triangle_MarshallJpaController(emf);
        Triangle_Marshall triangleOne = new Triangle_Marshall();
        triangleOne.setHeight(100);
        sm.create(triangleOne);
        triangleOne.setBase(100);
        sm.edit(triangleOne);
        sm.destroy(triangleOne.getId());
        List<Triangle_Marshall> myTriangles = sm.findTriangle_MarshallEntities();
            for(Triangle_Marshall myTriangle:myTriangles){
                System.out.println("Triangle Hieght"+ myTriangle.getHeight());
            }
        
    }
}
