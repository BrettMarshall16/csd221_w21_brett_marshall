package Lab4;

import Lab3.*;

/**
 *
 * @author 17052
 */
public class DiscMag extends Magazine {
    
    public DiscMag (String author, String title){
        super(author, title, 10);
    }
    public DiscMag (String author, String title, int orderQty){
        super(author, title, orderQty);
    }

    public DiscMag() {
    }
}
