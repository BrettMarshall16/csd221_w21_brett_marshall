/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

import Lab3.*;

/**
 *
 * @author 17052
 */
public class Book extends Publication {
    public String author;
    public Book(String author, int orderQty,String title,double price){
        this.author = author;
        this.copies = orderQty;
        this.title = title;
        this.price = price;
    };
    
     public String toString(){
        return "Title: " + title;
    };
    
}
