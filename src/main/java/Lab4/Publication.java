
package Lab4;

import Lab3.*;

/**
 *
 * @author 17052
 */
public class Publication {
    public String title;
    public String author;
    public double price;
    public int copies;

    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }
       public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Publication() {
    }
    


    public Publication(String title, String author, int copies) {
        this.title = title;
        this.author = author;
        this.copies = copies;
    }
    
}
