/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

import Lab3.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 17052
 */
public class App {
    ArrayList<Publication> bookList = new ArrayList<>();
    ArrayList<Publication> magazineList = new ArrayList();
    ArrayList<Magazine> discMagList = new ArrayList<>();
     public void run() throws Exception{
         
    
         DiscMag dm=new DiscMag("name", "title");
         DiscMag dm2=new DiscMag("name", "title", 12);
         DiscMag dm3=new DiscMag();
         dm3.setAuthor("author");
         dm3.setTitle("title");
         
        String theMenu = "1. Add A Book\n" + "2.Edit a Book\n" +
                         "3.Delete a Book\n" + "4.Add Magazine\n" +
                         "5.Add Disc Magazine \n" + "6.List Disc Magazine\n" + "7.Quit";
        boolean quit = false;
        Scanner input = new Scanner(System.in);
        
        while(!quit){
            System.out.println(theMenu);
            int option = input.nextInt();
            
            switch(option){
                case 1:
                    addBook();
                    break;
                case 2:
                    editBook();
                    break;
                case 3:
                    deleteBook();
                    break;
                case 4:
                    addMagazine();
                    break;
                case 5:
                    listMagazines();
                    break;
                case 6:
                    addDiscMagazine();
                    break;
                case 7:
                    listDiscMag();
                    break;
                case 8:
                    
                    
            }
            
        }
    }
     
     public void addBook(){
         Scanner input = new Scanner(System.in);
         System.out.println("Enter Author: ");
         String author = input.next();
         System.out.println("Quantity to Order: ");
         int orderQty = input.nextInt();
         System.out.println("Ener a Title: ");
         String title = input.next();
         System.out.println("Enter a Price: ");
         double price = input.nextDouble();
         Book x = new Book(author,orderQty,title,price);
         bookList.add(x);
     }
     public void listBooks(){
         int counter = 1;
         for (Publication Book : bookList){
             System.out.println(counter + "." + Book);
             counter++;
         }
     }
     public void editBook(){
         Scanner input = new Scanner(System.in);
         listBooks();
         System.out.println("Which book would you like to edit?: ");
         int index = input.nextInt();
         System.out.println("Enter a new Price: ");
         double price = input.nextDouble();
         bookList.get(index).setPrice(price);
     }
      public void deleteBook(){
         Scanner input = new Scanner(System.in);
         listBooks();
         System.out.println("Which book would you like to delete?: ");
         int index = input.nextInt();
         bookList.remove(index);
     }
     public void addMagazine(){
         Scanner input = new Scanner(System.in);
         System.out.println("Enter Author: ");
         String author = input.next();
         System.out.println("Ener a Title: ");
         String title = input.next();
         System.out.println("Quantity to Order: ");
         int orderQty = input.nextInt();
         Magazine x = new Magazine(author,title,orderQty);
         magazineList.add(x);
     }
     public void listMagazines(){
         int counter = 1;
         for (Publication Magazine : magazineList){
             System.out.println(counter + "." + Magazine);
             counter++;
         }
     }
     
     public void addDiscMagazine(){
         Scanner input = new Scanner(System.in);
         System.out.println("Enter Author: ");
         String author = input.next();
         System.out.println("Ener a Title: ");
         String title = input.next();
         DiscMag x = new DiscMag(author,title);
         discMagList.add(x);
     }
     public void listDiscMag(){
         int counter = 1;
         for (Publication DiscMag : discMagList){
             System.out.println(counter + "." + DiscMag);
             counter++;
         }
     }
     public void sellBook(){
         listBooks();
         Scanner input = new Scanner(System.in);
         System.out.println("Enter the book you want to sell: ");
         int x = input.nextInt();
     }
}
